#!/usr/bin/env python3.6

import re
import typing


import requests


drop_rates_location = "http://theprof9.webs.com/mmbn/droprates/mmbn3b.txt"
cache_location = "/tmp/mmbn3"

one_or_two_tabs = re.compile("\t\t?")
seperator = "-" * 72

ranks = [str(rank) for rank in range(11)] + ["S", "S+", "S++"]


try:
    with open(cache_location) as f:
        txt = f.read()
except IOError:
    txt = requests.get(drop_rates_location).content.decode("utf-8")
    with open(cache_location, "w") as f:
        f.write(txt)


class Enemy(typing.NamedTuple):
    name: str
    hp: str
    reward: str
    busting_level: str
    chance: str


def enemy_lines(text):
    enemies = text.split(seperator)[1:-1]  # First and last are junk

    for enemy in enemies:
        unfiltered_lines = enemy.splitlines()
        lines = list(filter(None, unfiltered_lines))

        split_lines = [
            one_or_two_tabs.split(line)
            for line in lines
        ]

        for line_number in range(len(split_lines)):
            for column_number in range(5):
                if split_lines[line_number][column_number] == "":
                    split_lines[line_number][column_number] = split_lines[line_number - 1][column_number]

        for line in split_lines:
            yield from expand_ranks(Enemy(*line))


def expand_ranks(enemy_line):
    if " - " not in enemy_line.busting_level:
        yield enemy_line
    else:

        start_rank, end_rank = enemy_line.busting_level.split(" - ")

        start_index, end_index = ranks.index(start_rank), ranks.index(end_rank)

        for rank in ranks[start_index: end_index + 1]:
            yield enemy_line._replace(busting_level=rank)


els = list(enemy_lines(txt))

els = [
    el
    for el in els
    if el.hp == ">=37.5%"
]


for el in els: print(f"name={el.name} reward={el.reward} rank={el.busting_level} chance={el.chance}")
