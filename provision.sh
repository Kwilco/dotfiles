#!/bin/bash

sudo add-apt-repository ppa:deadsnakes/ppa
sudo add-apt-repository ppa:fish-shell/release-3
sudo apt-add-repository -y ppa:system76-dev/stable

sudo apt-get update
sudo apt-get upgrade

sudo apt install \
    git \
    htop \
    openssh-server \
    nfs-common \
    nfs-server \
    terminator \
    vim \
    fish \
    python3.4 \
    python3.5 \
    python3.6 \
    python3.7 \
    jq \
    tree \
    silversearcher-ag \
    tmux \
    scrot \
    tig \
    system76-driver \
    system76-driver-nvidia


git config --global user.email kyle@kwil.co
git config --global user.name "Kyle Wilcox"
