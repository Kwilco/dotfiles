#!/usr/bin/env python3

import os

import requests


GITLAB_API = 'https://gitlab.com/api/v4'
PRIVATE_TOKEN = os.environ['GITLAB_PERSONAL_ACCESS_TOKEN']


def fetch_api_page(session: requests.Session, url: str, page_number: int) -> requests.Response:
    ''' Hit the GitLab API for a single page of a list API. '''

    response = session.get(
        url=url,
        params={
            'per_page': 100,
            'page': page_number,
        },
        stream=False,  # Force HTTP keep-alive
    )
    response.raise_for_status()
    return response


def fetch_all_api_pages(session: requests.Session, url: str):
    ''' Lazily return each API page HTTP response. '''

    total_pages = float('inf')
    current_page_number = 1

    while current_page_number <= total_pages:
        response = fetch_api_page(session, url, current_page_number)
        yield response

        total_pages = int(response.headers.get('x-total-pages', '1'))
        current_page_number += 1


def get_projects(group_id: str, private_token: str):
    ''' Lazily return all projects in the given namespace. `all` will return all projects. '''

    api_endpoint = 'projects' if group_id == 'all' else f'groups/{group_id}/projects'
    url = f'{GITLAB_API}/{api_endpoint}'
    session = requests.Session()
    session.headers.update({'PRIVATE-TOKEN': private_token})

    responses = fetch_all_api_pages(session, url)
    for response in responses:
        for project in response.json():
            yield project


def main():
    private_token = PRIVATE_TOKEN

    project_names = sorted(
        project['path_with_namespace']
        for project in get_projects('all', private_token)
    )

    for project_name in project_names:
        print(project_name)


if __name__ == '__main__':
    main()
