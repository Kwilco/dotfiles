
" Enable syntastic
" Hack filetype off because it apparently requires that
filetype off
call plug#begin('~/.vim/plugged')

" Syntastic - Highlights linting and syntax errors
Plug 'scrooloose/syntastic'


" Airline - powerline-ish status bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" CtrlP - fuzzy file finder
Plug 'kien/ctrlp.vim'

" Ansible syntax highlighting support
Plug 'chase/vim-ansible-yaml'

" Fix broken md highlighting
Plug 'tpope/vim-markdown'

" Python indentation support
Plug 'duckpunch/vim-python-indent'

" Highlights trailing whitespace
Plug 'bronson/vim-trailing-whitespace'

" Visual indent marker
Plug 'nathanaelkane/vim-indent-guides'

" Show source control status in gutter
Plug 'mhinz/vim-signify'

call plug#end()
filetype on


set shell=/bin/bash  "Set the shell to bash, because fish isn't supported properly
set t_Co=256  "256 vibrant terminal colors!!
set laststatus=2  "Always display the statusline in all windows

"4 space tabs
set tabstop=4
set softtabstop=4
set expandtab
set smarttab
set autoindent
set shiftwidth=4  "number of spaces to use for auto indent

" Un-fuck indentation for YAML files
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType yml setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType raml setlocal ts=2 sts=2 sw=2 expandtab

set backspace=indent,eol,start  "make backspaces more powerfull

set showmatch  "Highlight matching [{(
set incsearch  "Incremental search
set hlsearch  "Search results highlighting

set wildmenu  "Visual command autocomplete for vim commands

set showcmd "Show last command in lower right

set cursorline  "Highlight line cursor is on

if (exists('+colorcolumn'))
    set colorcolumn=100
    highlight ColorColumn ctermbg=9
    autocmd FileType markdown setlocal colorcolumn=80
endif

" CtrlP command options
let g:ctrlp_map = '<c-p>' " set Ctrl-P to launch ctrl P
let g:ctrlp_cmd = 'CtrlP' " set command line to CtrlP
let g:ctrlp_working_path_mode = 'ra' " set working path to current repo
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.git|\.hg|\.svn|node_modules)$',
  \ }

" Syntastic
let g:syntastic_check_on_open = 1
let g:syntastic_python_python_exec = '/home/kyle/.local/pipx/venvs/flake8/bin/python3'
let g:syntastic_python_flake8_args = ['-m', 'flake8']

" Airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='badwolf'
" " use powerline symbols
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
let g:airline#extensions#tabline#left_sep = ''
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''
let g:airline_powerline_fonts = 1

let g:signify_vcs_list = [ 'git' ]

colorscheme badwolf
"colorscheme apprentice

" change movement to jkl;
noremap ; l
noremap l k
noremap k j
noremap j h
